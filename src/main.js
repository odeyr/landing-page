import Vue from 'vue'
import VueMeta from 'vue-meta'
import router from './router'
import App from './App.vue'

Vue.config.productionTip = false

Vue.use(VueMeta, {
  // optional pluginOptions
  refreshOnceOnNavigation: true
})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
