import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home.vue'
import Patreons from './views/Patreons.vue'
import Terms from './views/legal/Terms.vue'
import Privacy from './views/legal/Privacy.vue'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  base: '/',
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
  routes: [
    {
      path: '/',
      name: 'home',
      alias: '/home',
      component: Home
    },
    {
      path: '/patreons',
      name: 'patreons',
      component: Patreons
    },
    {
      path: '/legal/terms',
      name: 'terms',
      component: Terms
    },
    {
      path: '/legal/privacy',
      name: 'privacy',
      component: Privacy
    },
    {
      path: '*',
      component: Home
    }
  ],
})

export default router
